"""
Written by: Daniel Nikoloski
e-mail: nikoloskid@pm.me

"""


from pyowm.owm import OWM
import requests


def retrieve_location():
    """
    Retrieves IP location

    :param city: extracts city from response
    :return: City
    """
    response = requests.get('https://ipapi.co/json/', timeout=5)
    data = response.json()
    # Extract the city from the response
    city = data['city']
    return city


def weather_api(city):
    """
    Retrieves weather data for the specified city.

    :param city: The name of the city to fetch weather data for.
    :return: The weather data for the specified city.
    """
    owm = OWM('e839324c275934d5a85a7822f23e3aa2')  # API-KEY
    weather_mngr = owm.weather_manager()
    observation = weather_mngr.weather_at_place(city)
    weather_data = observation.weather
    return weather_data


def print_weather(weather_data):
    """
    Prints weather data.
    """
    temperature = weather_data.temperature('celsius')["temp"]
    humidity = weather_data.humidity
    wind_dict_in_meters_per_sec = weather_data.wind()["speed"]
    print(f'The temperature is: {temperature}°C')
    print(f'Rain percentage is: {humidity}%')
    print(f'Wind speed is: {wind_dict_in_meters_per_sec}m/s')


def get_weather_data_for_new_city():
    """
    Ask for for new city to retrieve data from.
    """
    new_city = input('Enter the name of another city: ')
    city_weather_data = weather_api(new_city)
    print_weather(city_weather_data)


def main():
    """
    Prints weather data extracted from the retrieved location.
    Prints weather data for new city that is inputed my the user.
    """
    city = retrieve_location()
    print(f'Retrieving weather information for your location {city}...\n')
    weather_data = weather_api(city)
    print_weather(weather_data)

    new_city_data = input('Would you like weather data for another city? (yes/no): ')
    if new_city_data.lower() == 'yes':
        get_weather_data_for_new_city()


if __name__ == '__main__':
    main()
